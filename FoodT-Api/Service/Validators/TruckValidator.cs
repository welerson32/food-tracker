namespace Service.Validators
{
    using System;
    using Domain.Entities;
    using FluentValidation;

    public class TruckValidator : AbstractValidator<Truck>
    {
        public TruckValidator()
        {
	    RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("Can't found the object.");
                });
		
            RuleFor(c => c.CpfCnpj)
                .NotEmpty().WithMessage("Is necessary to inform the CPF/CNPJ.")
                .NotNull().WithMessage("Is necessary to inform the CPF/CNPJ.");

            RuleFor(c => c.Email)
                .NotEmpty().WithMessage("Is necessary to inform the email.")
                .NotNull().WithMessage("Is necessary to inform the email.");

            RuleFor(c => c.PersonType)
                .NotEmpty().WithMessage("Is necessary to inform the person type.")
                .NotNull().WithMessage("Is necessary to inform the person type.");

            RuleFor(c => c.Phone1)
                .NotEmpty().WithMessage("Is necessary to inform the phone number.")
                .NotNull().WithMessage("Is necessary to inform the phone number.");

            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Is necessary to inform the name.")
                .NotNull().WithMessage("Is necessary to inform the name.");
        }
    }
}
