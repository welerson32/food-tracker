namespace Service.Validators
{
    using System;
    using Domain.Entities;
    using FluentValidation;

    public class ClientValidator : AbstractValidator<Client>
    {
        public ClientValidator()
        {
	    RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("Can't found the object.");
                });
		
            RuleFor(c => c.Cpf)
                .NotEmpty().WithMessage("Is necessary to inform the CPF.")
                .NotNull().WithMessage("Is necessary to inform the CPF.");

            RuleFor(c => c.Email)
                .NotEmpty().WithMessage("Is necessary to inform the email.")
                .NotNull().WithMessage("Is necessary to inform the email.");

            RuleFor(c => c.Phone1)
                .NotEmpty().WithMessage("Is necessary to inform the phone number.")
                .NotNull().WithMessage("Is necessary to inform the phone number.");

            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Is necessary to inform the name.")
                .NotNull().WithMessage("Is necessary to inform the name.");
        }
    }
}
