namespace Domain.Entities
{
    public class Truck : BaseEntity
    {
        public int TruckId { get; set; }
        public string Name { get; set; }
        public string OwnerName { get; set; }
        public string FoodType { get; set; }
        public string Descryptions { get; set; }
        public string CpfCnpj { get; set; }
        public string PersonType { get; set; }
        public string Email { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
    }
}
