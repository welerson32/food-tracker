namespace Domain.Entities
{
    public class Client : BaseEntity
    {
        public int ClientId { get; set; }
        public string Name { get; set; }
        public string BirthDate { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
    }
}
