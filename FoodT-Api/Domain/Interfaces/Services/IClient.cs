namespace Domain.Interfaces.Services
{
    using System.Collections.Generic;
    using Domain.Entities;
    using FluentValidation;

    public interface IClient<T> where T : BaseEntity
    {
        T Post<V>(T obj) where V : AbstractValidator<T>;
        T Put<V>(T obj) where V : AbstractValidator<T>;
        void Delete(int id);
        T GetById(int id);
        IList<T> GetAllAsync();
    }
}
