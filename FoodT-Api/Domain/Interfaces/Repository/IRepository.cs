namespace Domain.Interfaces.Repository
{
    using System.Collections.Generic;
    using Domain.Entities;

    public interface IRepository<T> where T : BaseEntity
    {
        void Insert(T obj);
        void Update(T obj);
        void Remove(int id);
        T Select(int id);
        IList<T> SelectAll();
    }
}
