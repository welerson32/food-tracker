namespace Data.Context
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    
    public class ApplicationContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=my_host;Database=my_db;Username=my_user;Password=my_pw");

        public DbSet<Client> Clients { get; set; }
        public DbSet<Truck> Trucks { get; set; }
    }
}
