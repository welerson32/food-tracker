namespace Data.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using Data.Context;
    using Domain.Entities;
    using Domain.Interfaces.Repository;

    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private ApplicationContext context = new ApplicationContext();

        public void Insert(T obj)
        {
            context.Set<T>().Add(obj);
            context.SaveChanges();
        }
        
        public void Update(T obj)
        {
            context.Entry(obj).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            context.Set<T>().Remove(Select(id));
            context.SaveChanges();
        }
        public T Select(int id)
        {
            return context.Set<T>().Find(id);
        }
        public IList<T> SelectAll()
        {
            return context.Set<T>().ToList();
        }
    }
}
