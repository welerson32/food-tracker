import { Component, OnInit,} from '@angular/core';
import { HomeService } from './../service/service.service';

declare let google: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./../../app.component.css']
})

export class HomeComponent implements OnInit {

  constructor(private Service: HomeService) { }

  lat = -19.918622875284022;
  lng = -43.93859346530122;

  async ngOnInit() {
    await this.Service.GetGeoLocation().then(pos => {
      this.lat = pos.lat; 
      this.lng = pos.lng;
    });
    this.initMap();
  }

  markOnClick(event){
    console.log(event);
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
  }

  initMap() {
    // The location of Uluru
    var uluru = { lat: this.lat, lng: this.lng };
    // The map, centered at Uluru
    var map = new google.maps.Map(
        document.getElementById('map'), { zoom: 16.5, center: uluru });
    // The marker, positioned at Uluru
    var marker = new google.maps.Marker({ position: uluru, map: map });
}

}
